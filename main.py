from collections import Counter  

input = "input_e.txt"
output = "solution_e.txt"

def sort_by_frequency(lst):  
  freq_dict = Counter(lst) 
  return sorted(lst, key=lambda x: (-freq_dict[x], x))  

data = open(input).read().splitlines()
noOfClients = int(data[0])
# print(noOfClients)
data = data[1::]
# print(data)
likedIngredients = []
dislikedIngredients = []
for idx,x in enumerate(data):
  ingredients = x.split(" ")[1::]
  if idx%2 == 0:
    likedIngredients += ingredients
  else:
    dislikedIngredients += ingredients

# print(f"Sorted likedIngredients = {sort_by_frequency(likedIngredients)}")  

# print(f"Sorted dislikeIngredients = {sort_by_frequency(dislikedIngredients)}") 

likedNoDuplicates = list(dict.fromkeys(likedIngredients)) 
dislikeNoDuplicates = list(dict.fromkeys(dislikedIngredients)) 

ingredients = []
# Rules:
# If likes is +3 more than dislikes
# if no dislikes, then add
for x in likedNoDuplicates:
  if x not in dislikedIngredients:
    ingredients.append(x)
    continue
  likeCnt = likedIngredients.count(x)
  dislikeCnt = dislikedIngredients.count(x)
  if likeCnt - dislikeCnt >= 1:
    ingredients.append(x)
  
ingredientsCnt = len(ingredients)
outputStr = str(ingredientsCnt) + " " + " ".join(ingredients)
print(outputStr) 

f = open(output,"w")
f.write(outputStr)
f.close()